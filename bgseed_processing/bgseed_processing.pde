import processing.serial.*; 

String serial;                 // Declare a new string called 'serial' . A string is a sequence of characters (data type know as "char")
Serial port;                   // The serial port, this is a new instance of the Serial class (an Object)
int end = 10;                  // the number 10 is ASCII for linefeed (end of serial.println), later we will look for this to break up individual messages

/*
  The sensors:
 sensorValues[0] is LDR1
 sensorValues[1] is LDR2
 sensorValues[2] is Motion
 */
int[] sensorValues;

// Constants
int BACKGROUND = 1;

// Canvas variables


void setup() {
  printArray(Serial.list());                         // List connected devices 
  port = new Serial(this, Serial.list()[2], 9600);   // Initializing the object by assigning a port and baud rate (must match that of Arduino)
  port.clear();                                      // Function from serial library that throws out the first reading, in case we started reading in the middle of a string from Arduino
  serial = port.readStringUntil(end);                // Function that reads the string from serial port until a println and then assigns string to our string variable (called 'serial')
  serial = null;                                     // Initially, the string will be null (empty)

  // Processing setup settings
  size(800, 600); // Params width, height
  frameRate(30);
  background(BACKGROUND);
}

void draw() {
  getSensorValues();

  // Only draw, if there are sensorValues
  if (sensorValues != null) {
    background(BACKGROUND);
    
    float average = (sensorValues[0] + sensorValues[1]) / 2;
    float mappedSensorValue0 = map(average, 9, 520, 0, height);
    stroke(#ff0000);
    ellipse(width / 2, height / 2, mappedSensorValue0, mappedSensorValue0);
  }
}

void getSensorValues() {
  while (port.available() > 0) { 
    serial = port.readStringUntil(end);
  }
  if (serial != null) {
    sensorValues = int(splitTokens(serial));
  }
}

void printSensorValues() {
  if (sensorValues != null) {
    println(sensorValues);
    println("------------------");
  }
}