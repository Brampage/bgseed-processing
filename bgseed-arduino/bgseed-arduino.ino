int sensors[] = { A0, A1 };
int numberOfSensors = 2;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println(retrieveSensorValues(sensors));
}

String retrieveSensorValues(int sensors[]) {
  String values = "";
  for (int i = 0; i < numberOfSensors; i++) {
    values += String(analogRead(sensors[i]));
    if (i != numberOfSensors - 1) {
      values += " ";
    }
  }
  return values;
}

